package com.restore.measurements.client

import com.restore.grpc.client.{ChannelConfig, ChannelFactory}
import com.restore.grpc.security.auth.client.AdminTokenAuthenticationProvider
import com.restore.measurements.grpc.MeasurementsRequest.Source.Key
import com.restore.measurements.grpc.{MeasurementsRequest, MeasurementsServiceApi}
import com.restore.time2.DateTimeConversions._
import io.grpc.{CallCredentials, ManagedChannel, Metadata}
import monix.eval.Task
import monix.execution.Scheduler.Implicits.global
import org.joda.time.DateTime.now

import java.util.concurrent.Executor

object MeasurementsClient {

  def main(args: Array[String]): Unit = {

    val key = "CAL1BER.PROCESS.UP_ENGINE..POWER"
    val config = ChannelConfig("measurements.qa.flexpond.net", adminToken = Some("REstore"))
    val channel: ManagedChannel = ChannelFactory(config).channel()

    val request = MeasurementsRequest(
      source = Key(key),
      start = Some(now() - 5.minutes),
      end = Some(now()),
      at = Some(now()),
      onlyLatest = true,
      aggregation = None
    )

    println("Fetching...")
    stub(channel, config)
      .fetchMeasurements(request)
      .runSyncUnsafe(1.minute)
      .measurements
      .foreach(m => {
        println(s"FETCH ${m.timestamp} / ${m.inserted} / ${m.value}")
      })

    println("Streaming...")
    stub(channel, config)
      .streamMeasurements(request)
      .take(10)
      .doOnNext(m => {
        Task(println(s"STREAM ${m.timestamp} / ${m.inserted} / ${m.value}"))
      })
      .toListL
      .runSyncUnsafe(1.minute)

    channel.shutdownNow()

  }

  private def stub(
      channel: ManagedChannel,
      config: ChannelConfig
    ): MeasurementsServiceApi.MeasurementsServiceApiStub = {
    MeasurementsServiceApi
      .stub(channel)
      .mapCallOptions(co => Task(co.withCallCredentials(AdminTokenCallCredentials(config.adminToken.get))))
  }

}

case class AdminTokenCallCredentials(adminToken: String) extends CallCredentials {
  override def thisUsesUnstableApi(): Unit = {}

  override def applyRequestMetadata(
      requestInfo: CallCredentials.RequestInfo,
      appExecutor: Executor,
      applier: CallCredentials.MetadataApplier
    ): Unit = applier.apply(
    AdminTokenAuthenticationProvider(adminToken).addAuthentication(new Metadata)
  )
}
